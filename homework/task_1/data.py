from models import Ingredient, Recipe, RecipeIngredient

recipes = [
    {
        "id": 1,
        "name": "Сырники из творога",
        "description": "Главный секрет идеальных сырников — а точнее творожников, — творог нужно протереть через "
        "мелкое сито и отжать от влаги. Жирность предпочтительна не больше и не меньше 9%.",
        "cooking_time": 30,
        "views": 10,
    },
    {
        "id": 2,
        "name": "Спагетти карбонара с красным луком",
        "description": "Спагетти карбонара — хоть блюдо и итальянское, оно имеет хорошую популярность во всем мире, "
        "в том числе и у нас. Изобретенная когда-то простыми шахтерами, эта простая и сытная паста "
        "завоевала сердца и желудки многих.",
        "cooking_time": 20,
        "views": 5,
    },
]

initial_recipes = [Recipe(**recipe) for recipe in recipes]

ingredients = [
    {"id": 1, "name": "Творог"},
    {"id": 2, "name": "Куриное яйцо"},
]

initial_ingredients = [Ingredient(**ingredient) for ingredient in ingredients]

recipe_ingredients = [
    {"recipe_id": 1, "ingredient_id": 1},
    {"recipe_id": 1, "ingredient_id": 2},
]

initial_dish_ingredients = [
    RecipeIngredient(**ingredient) for ingredient in recipe_ingredients
]
