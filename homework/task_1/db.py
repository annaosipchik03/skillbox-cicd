"""
Первый - он видит таблицу со списком всех рецептов в базе. В таблице такие поля:
название
количество просмотров
время готовки (в минутах)


Рецепты отсортированы по популярности (количество просмотров - сколько раз открыли детальный рецепт) -
чем чаще открывают рецепт, тем он популярнее. В случае совпадения значений сортировать по времени готовки.


Второй - он видит детальную информацию по каждому рецепту:
название
время готовки
список ингредиентов
тексnовое описание

"""
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

path = "sqlite+aiosqlite:///recipe.db"
engine = create_async_engine(path, echo=True)
Base = declarative_base(bind=engine)
async_session = sessionmaker(bind=engine, expire_on_commit=False, class_=AsyncSession)
