from typing import List, Optional

from data import initial_dish_ingredients, initial_ingredients, initial_recipes
from db import AsyncSession, Base, async_session, engine
from fastapi import Depends, FastAPI, Path
from models import Ingredient, Recipe
from schemas import RecipeIn, RecipeOut
from sqlalchemy import update
from sqlalchemy.future import select

app = FastAPI()


async def get_session() -> AsyncSession:
    async with async_session() as session:
        yield session


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
        async with async_session() as session:
            dishes = await session.execute(select(Recipe))
            if not dishes.fetchall():
                session.add_all(initial_recipes)
                session.add_all(initial_ingredients)
                session.add_all(initial_dish_ingredients)
            await session.commit()


@app.on_event("shutdown")
async def shutdown():
    await engine.dispose()


@app.get("/recipes", response_model=List[RecipeOut])
async def get_recipes(session: AsyncSession = Depends(get_session)) -> List[Recipe]:
    statement = (
        select(Recipe).order_by(Recipe.views.desc()).order_by(Recipe.cooking_time)
    )
    # import pdb;pdb.set_trace()
    result = await session.execute(statement)
    return result.scalars().all()


@app.get("/recipes/{idx}", response_model=RecipeOut)
async def get_recipe(
    idx: int = Path(default=1, title="Idx of the recipe"),
    session: AsyncSession = Depends(get_session),
) -> Optional[Recipe]:
    result = await session.execute(select(Recipe).where(Recipe.id == idx))
    await session.execute(
        update(Recipe).where(Recipe.id == idx).values(views=Recipe.views + 1)
    )
    await session.commit()
    return result.scalars().first()


@app.post("/recipes", response_model=RecipeOut)
async def post_recipe(
    recipe: RecipeIn, session: AsyncSession = Depends(get_session)
) -> Recipe:
    ingredients_raw = recipe.ingredients
    ingredients_new = [
        Ingredient(**ingredient.dict()) for ingredient in ingredients_raw
    ]
    # import pdb; pdb.set_trace()
    data = recipe.dict()
    data.pop("ingredients")
    recipe = Recipe(**data)
    id = recipe.id
    # for el in ingredients: session.add(el)
    # await session.commit()
    # recipe.ingredients.append(ingredients[0])
    # import pdb; pdb.set_trace()

    session.add(recipe)
    session.add_all(ingredients_new)
    await session.commit()
    return recipe
