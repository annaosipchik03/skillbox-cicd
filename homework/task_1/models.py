"""
Первый - он видит таблицу со списком всех рецептов в базе. В таблице такие поля:
название
количество просмотров
время готовки (в минутах)


Рецепты отсортированы по популярности (количество просмотров - сколько раз открыли детальный рецепт) -
чем чаще открывают рецепт, тем он популярнее. В случае совпадения значений сортировать по времени готовки.


Второй - он видит детальную информацию по каждому рецепту:
название
время готовки
список ингредиентов
тексnовое описание

"""
from db import Base
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import relationship


class Recipe(Base):
    __tablename__ = "recipe"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False, index=True)
    views = Column(Integer, default=0)
    description = Column(String, nullable=False)
    cooking_time = Column(Integer, nullable=False)
    ingredients = relationship(
        "RecipeIngredient", back_populates="recipe", cascade="all"
    )


class RecipeIngredient(Base):
    __tablename__ = "ri"
    id = Column(Integer, primary_key=True)
    recipe_id = Column(Integer, ForeignKey("recipe.id"), nullable=False)
    ingredient_id = Column(Integer, ForeignKey("ingredient.id"), nullable=False)
    recipe = relationship("Recipe", back_populates="ingredients")
    ingredient = relationship("Ingredient", back_populates="recipes")


class Ingredient(Base):
    __tablename__ = "ingredient"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False, index=True)
    recipes = relationship(
        "RecipeIngredient", back_populates="ingredient", cascade="all"
    )
