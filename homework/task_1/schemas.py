from typing import List, Union

from pydantic import BaseModel, Field


class BaseIngredients(BaseModel):
    name: str = Field(title="Name of ingredient", min_length=1, max_length=100)


class IngredientsIn(BaseIngredients):
    ...


class IngredientsOut(BaseIngredients):
    id: int

    class Config:
        orm_mode = True


class BaseRecipe(BaseModel):
    """
    class Recipe(Base):
        __tablename__ = "recipe"
        id = Column(Integer, primary_key=True, index=True)
        name = Column(String, nullable=False, index=True)
        views = Column(Integer, default=0)
        description = Column(String, nullable=False)
        cooking_time = Column(Integer, nullable=False)
        r = relationship("RecipeIngredient", back_populates="recipe", lazy="joined")
        ingredients = association_proxy("r", "ingredients")
    """

    name: str = Field(title="Name of the recipe", min_length=5, max_length=100)
    views: int = Field(title="Views on the recipe", default=0, ge=0)
    description: str = Field(
        title="Description of teh recipe", min_length=10, max_length=1000
    )
    cooking_time: int = Field(title="Cooking time of the recipe", gt=0)


class RecipeIn(BaseRecipe):
    ingredients: Union[List[IngredientsIn], List[IngredientsOut]] = Field(
        title="Ingredient for the recipe"
    )


class RecipeOut(BaseRecipe):
    id: int

    class Config:
        orm_mode = True
